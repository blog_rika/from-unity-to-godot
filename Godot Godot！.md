### 部分教程来源

-  [timothyqiu的个人空间_bilibili](https://space.bilibili.com/7092)

### 拖拽节点到脚本里可以直接声明对应的引用变量

如题

### 属性面板本地化

在属性面板右上角的设置中可以显示属性的名称，分别是【原始】【首字母大写】【本地化】，

选择本地化后即可看到中文的属性面板：

![[Pasted image 20230122234241.png]]

当然也可以选择【原始】来看查看代码中需要用的名称。

### 编辑器脚本

在脚本第一行添加关键字 `@tool` 表示这个脚本会在编辑器中运行，使用 `Engine.is_editor_hint()` 判断当前是否是在编辑器模式下运行。

```ad-tip
修改编辑器脚本后，需要重新加载场景才能让其生效（也可能需要重启项目）
```

让一个脚本继承 `EditorScript` 并在其中添加 `_run`  生命周期方法即可实现一个不依赖节点的编辑器脚本，在脚本编辑器的菜单栏【文件】【运行】处执行它。

### setget

等同于 c# 的封装器：

```python
@export var text:String : set = sett, get = gett

func sett(val):
	print(val)

func gett():
	return text
```

```ad-tip
godot 3 的写法是：

`export var text:String setget sett, gett`
```

还可以写匿名函数：

```python
@tool
extends Label

@export var newText:String:
	set(val):
		newText = val
		text = val
	get:
		return newText


func _run():
	print(123)

```

### 复制资源

来源：[How do I duplicate a resource and set its contents by code? - Godot Engine - Q&A](https://godotengine.org/qa/136894/how-do-i-duplicate-a-resource-and-set-its-contents-by-code)

```python
var cloned_resource = my_resource.duplicate()
```

```ad-tip
在我的项目中，我把一个材质球做模板，复制后，贴上贴图来快速实现不同角色的皮肤。
```

### 给 export 的变量分组：

C# 例子如下：

```csharp
[ExportGroup("titide")] [Export] private int a;  
[ExportCategory("mc")][Export] private int b;  
[ExportSubgroup("mljadiwoc")][Export] private int c;
```

![[Pasted image 20230129180902.png]]

### owner 和 duplicate()

duplicate 可以复制一个节点，但不会复制 owner 属性。

findChild 方法默认只寻找 owner 属性为当前节点的子节点，可以通过 owner 参数更改。

### Godot 4 中的 C# 信号处理

```ad-tip
从这里开始我又沉迷 C# 了......
```

C# 中使用 `[Signal]` 特征标记一个 `delegate` 来表示 Godot 信号，这个 `delegate` 的名称应该以 `EventHandler` 结尾，这样 Godot 就会自动生成一个同名但去除 `EventHandler` 后缀的 C# 事件（`event`）：

```csharp
[Signal]  
public delegate void OnActivatedEventHandler(ListMenuItem item);

public void _Ready(){
	// 这里的 OnActivated 就是自动生成的 event 了。
	EmitSignal(nameof(OnActivated), _items[SelectIndex]);
}
```

```ad-tip
Godot 提供这个 `event` 是为了方便在 C# 中订阅事件，也就是希望让咱们使用 `OnActivated += SomeMethod` 的形式代替原来的 `Connect` 方法。

但如果要触发事件，还是需要使用 `EmitSignal`方法， 目前还不支持直接调用 `event` 的 `Invork`。
```

### Visible 和 Process Mode

Unity 当中经常用 `SetActive(false)` 隐藏物体，而 Godot 就是用 `Visible = false` 隐藏了。

但是，Unity 中被隐藏的物体是不会执行 `Update` 方法的，而 Godot 中隐藏后的物体依旧会执行 `_Process` 方法，这时候就需要单独设置一下 `ProcessMode` 来暂停 `_Process` 方法了：

```csharp
ProcessMode = ProcessModeEnum.Disabled;
```

`ProcessMode` 默认是 `Inherit`，也就是继承父节点的 `ProcessMode`。实际效果就类似 Unity 中给父物体设置 `SetActive(false)` 也会同时停止子物体的 `Update` 一样。